//
//  Item.swift
//  ToDoey
//
//  Created by Artem Burmistrov on 2018-09-10.
//  Copyright © 2018 Artem Burmistrov. All rights reserved.
//

import Foundation

class Item {
    
    var title: String = ""
    var done: Bool = false

}
